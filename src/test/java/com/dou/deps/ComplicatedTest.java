package com.dou.deps;

import com.dou.deps.result.FieldTree;
import com.dou.deps.result.FieldsAnalysisResult;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

import static com.dou.deps.analyzer.MethodAnalyzer.analyzeFields;
import static java.util.Collections.emptySet;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ComplicatedTest {

    public static class Hello implements Runnable {

        private int a;
        private int b;
        private int c;
        private int d;
        private Hello child;
        private boolean flag;
        private long someLong;
        private float str;
        private Foo foo;

        public int world(boolean b1, boolean b2) {
            int i = child.a;
            Hello h = child.child;
            if (b1) {
                h = this;
                h.a += child.b;
                i = c;
            }
            Hello loopedRef = this;
            while (ThreadLocalRandom.current().nextBoolean()) {
                loopedRef = loopedRef.child;
            }
            System.out.println(loopedRef.d);
            child.run();
            if (b2) {
                foo.bar();
                return i;
            }
            Supplier<AtomicInteger> call = foo;
            System.out.println(call.get().get());
            return (int) (h.a + getThisOrChild().someLong);
        }

        private Hello getThisOrChild() {
            if (flag)
                return this;
            else
                return child;
        }

        @Override
        public void run() {
            System.out.println(str);
        }
    }

    public class Foo implements Supplier<AtomicInteger> {
        private String str;
        private AtomicInteger ai;
        private Bar bar;

        public int bar() {
            int i = bar.i;
            return i + str.hashCode();
        }

        @Override
        public AtomicInteger get() {
            return ai;
        }
    }

    public class Bar {
        public int i;
    }

    @Test
    public void test() throws NoSuchMethodException, NoSuchFieldException {
        Method world = Hello.class.getDeclaredMethod("world", boolean.class, boolean.class);

        Field a = Hello.class.getDeclaredField("a");
        Field b = Hello.class.getDeclaredField("b");
        Field c = Hello.class.getDeclaredField("c");
        Field d = Hello.class.getDeclaredField("d");
        Field child = Hello.class.getDeclaredField("child");
        Field flag = Hello.class.getDeclaredField("flag");
        Field someLong = Hello.class.getDeclaredField("someLong");
        Field str = Hello.class.getDeclaredField("str");
        Field foo = Hello.class.getDeclaredField("foo");
        Field foo_str = Foo.class.getDeclaredField("str");
        Field foo_ai = Foo.class.getDeclaredField("ai");
        Field foo_bar = Foo.class.getDeclaredField("bar");
        Field bar_i = Bar.class.getDeclaredField("i");
        Field string_value = String.class.getDeclaredField("value");
        Field string_hash = String.class.getDeclaredField("hash");
        Field string_coder = String.class.getDeclaredField("coder");
        Field atomicint_value = AtomicInteger.class.getDeclaredField("value");

        FieldTree expectedUsedFields = new FieldTree();
        expectedUsedFields.getOrAdd(a);
        expectedUsedFields.getOrAdd(c);
        expectedUsedFields.getOrAdd(flag);
        expectedUsedFields.getOrAdd(someLong);
        expectedUsedFields.getOrAdd(d);
        expectedUsedFields.getOrAdd(child).getOrAdd(a);
        expectedUsedFields.getOrAdd(child).getOrAdd(b);
        expectedUsedFields.getOrAdd(child).getOrAdd(someLong);
        expectedUsedFields.getOrAdd(child).getOrAdd(str);
        expectedUsedFields.getOrAdd(child).getOrAdd(d);
        expectedUsedFields.getOrAdd(foo).getOrAdd(foo_str).getOrAdd(string_value);
        expectedUsedFields.getOrAdd(foo).getOrAdd(foo_str).getOrAdd(string_hash);
        expectedUsedFields.getOrAdd(foo).getOrAdd(foo_str).getOrAdd(string_coder);
        expectedUsedFields.getOrAdd(foo).getOrAdd(foo_ai).getOrAdd(atomicint_value);
        expectedUsedFields.getOrAdd(foo).getOrAdd(foo_bar).getOrAdd(bar_i);

        FieldsAnalysisResult result = new FieldsAnalysisResult(analyzeFields(world));
        System.out.println(result);

        assertEquals(emptySet(), result.getReturnedCompositeRefs());
        assertEquals(expectedUsedFields, result.getUsedFields());
    }
}
