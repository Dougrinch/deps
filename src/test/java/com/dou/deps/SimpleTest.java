package com.dou.deps;

import com.dou.deps.analyzer.Configuration;
import com.dou.deps.result.FieldTree;
import com.dou.deps.result.FieldsAnalysisResult;
import com.dou.deps.value.Ref;
import com.dou.deps.value.Value;
import lombok.Data;
import lombok.val;
import org.junit.jupiter.api.Test;

import java.lang.annotation.Retention;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.dou.deps.analyzer.FieldPredicate.withAnnotations;
import static com.dou.deps.analyzer.MethodAnalyzer.analyzeFields;
import static com.dou.deps.value.Value.typedValue;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.objectweb.asm.Type.getType;
import static org.springframework.util.CollectionUtils.lastElement;

public class SimpleTest {

    @Retention(RUNTIME)
    @interface Column {
    }

    @Retention(RUNTIME)
    @interface Join {
    }

    interface HasId {
        long getId();
    }

    @Data
    public static class Account implements HasId {
        @Column
        public long id;
        @Column
        public long balance;
    }

    @Data
    public static class Client implements HasId {
        @Column
        public long id;
        @Column
        public String name;
        @Join
        public Account defaultAccount;

        public transient String runtimeInfo;
    }

    @Data
    public static class Card implements HasId {
        @Column
        public long id;
        @Column
        public String msisdn;
        @Join
        public Account account;
        @Join
        public Client client;

        public long getClientIdDirect() {
            return client.id;
        }

        public long getClientIdWithGetters() {
            return getClient().getId();
        }

        public long getClientIdWithObjectAndCast() {
            Object c = client;
            return ((HasId) c).getId();
        }

        public Long getClientIdOrAccountIdOrNull() {
            if (client != null)
                return client.getId();
            else if (account != null)
                return account.getId();
            else
                return null;
        }

        public long getBalance() {
            Account acc;
            if (account != null)
                acc = account;
            else
                acc = client.getDefaultAccount();
            return acc.getBalance();
        }

        public Account getActualAccount() {
            return internalGetActualAccount();
        }

        private Account internalGetActualAccount() {
            if (account != null)
                return account;
            else
                return client.getDefaultAccount();
        }

        public String getClientRuntimeInfo() {
            return getClient().getRuntimeInfo();
        }
    }

    @Test
    public void testSimpleGetter() {
        val result = analyze("getAccount");
        assertReturned(result, "account");
        assertUsed(result);
    }

    @Test
    public void testNestedFields() {
        val result = analyze("getClientIdDirect");
        assertReturned(result);
        assertUsed(result, "client.id");
    }

    @Test
    public void testNestedGetters() {
        val result = analyze("getClientIdWithGetters");
        assertReturned(result);
        assertUsed(result, "client.id");
    }

    @Test
    public void testFlowSplit() {
        val result = analyze("getClientIdOrAccountIdOrNull");
        assertReturned(result);
        assertUsed(result, "client.id", "account.id");
    }

    @Test
    public void testFlowMerge() {
        val result = analyze("getBalance");
        assertReturned(result);
        assertUsed(result, "account.balance", "client.defaultAccount.balance");
    }

    @Test
    public void ignoreNotInterestingField() {
        val result = analyze("getClientRuntimeInfo");
        assertReturned(result);
        assertUsed(result);
    }

    @Test
    public void testCastOverObject() {
        val result = analyze("getClientIdWithObjectAndCast");
        assertReturned(result);
        assertUsed(result, "client.id");
    }

    @Test
    public void testPrivateMethodCall() {
        val result = analyze("getActualAccount");
        assertReturned(result, "account", "client.defaultAccount");
        assertUsed(result);
    }

    private FieldsAnalysisResult analyze(String methodName) {
        try {
            return new FieldsAnalysisResult(analyzeFields(Card.class.getDeclaredMethod(methodName), new Configuration(withAnnotations(Join.class), withAnnotations(Column.class))));
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    private void assertReturned(FieldsAnalysisResult result, String... values) {
        assertEquals(returned(values), result.getReturnedCompositeRefs());
    }

    private void assertUsed(FieldsAnalysisResult result, String... values) {
        assertEquals(used(values), result.getUsedFields());
    }

    private Set<Ref> returned(String... values) {
        return stream(values).map(this::ref).collect(toSet());
    }

    private FieldTree used(String... stringRefs) {
        Set<Value> values = new HashSet<>();
        for (String stringRef : stringRefs) {
            Ref ref = ref(stringRef);
            values.add(typedValue(getType(lastElement(ref.getPath()).getType()), ref));
        }
        return new FieldTree(values);
    }

    private Ref ref(String fieldPath) {
        try {
            Class<?> clazz = Card.class;
            List<Field> fields = new ArrayList<>();
            for (String fieldName : fieldPath.split("\\.")) {
                Field field = clazz.getDeclaredField(fieldName);
                fields.add(field);
                clazz = field.getType();
            }
            return new Ref(fields, true);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }
}
