package com.dou.deps.analyzer;

import java.lang.reflect.Field;

public class Configuration {

    private final FieldPredicate compositeFieldPredicate;
    private final FieldPredicate interestingFieldPredicate;

    public Configuration(FieldPredicate compositeFieldPredicate, FieldPredicate interestingFieldPredicate) {
        this.compositeFieldPredicate = compositeFieldPredicate;
        this.interestingFieldPredicate = interestingFieldPredicate;
    }

    public boolean isCompositeField(Field field) {
        return compositeFieldPredicate.test(field);
    }

    public boolean isInterestingField(Field field) {
        return interestingFieldPredicate.test(field);
    }
}
