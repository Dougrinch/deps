package com.dou.deps.analyzer;

import com.dou.deps.utils.MethodReader.MethodInfo;
import com.dou.deps.value.Value;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.analysis.Analyzer;
import org.objectweb.asm.tree.analysis.AnalyzerException;
import org.objectweb.asm.tree.analysis.BasicValue;
import org.objectweb.asm.tree.analysis.Frame;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Set;

import static com.dou.deps.utils.MethodReader.readMethod;
import static com.dou.deps.value.Value.mergeValues;
import static org.objectweb.asm.Opcodes.*;

public class MethodAnalyzer {

    public static class Result {
        private final Set<Value> usedFields;
        private final Value returnedCompositeValue;

        public Result(Set<Value> usedFields, Value returnedCompositeValue) {
            this.usedFields = usedFields;
            this.returnedCompositeValue = returnedCompositeValue;
        }

        public Set<Value> getUsedFields() {
            return usedFields;
        }

        public Value getReturnedCompositeValue() {
            return returnedCompositeValue;
        }
    }

    public static Result analyzeFields(Method method) {
        return analyzeFields(method, new Configuration(FieldPredicate.all(), FieldPredicate.all()));
    }

    public static Result analyzeFields(Method method, Configuration configuration) {
        if (Modifier.isNative(method.getModifiers()))
            throw new IllegalStateException("could not analyze native method " + method);

        MethodInfo methodInfo = readMethod(method);

        MethodNode mn = methodInfo.getMethodNode();
        String internalClassName = methodInfo.getInternalDeclaringClassName();
        int classAccess = methodInfo.getClassAccess();

        Context context = new Context(method, classAccess);
        FieldsInterpreter interpreter = new FieldsInterpreter(context, configuration);
        Analyzer<BasicValue> analyzer = new Analyzer<>(interpreter);
        try {
            analyzer.analyze(internalClassName, mn);
        } catch (AnalyzerException e) {
            throw new RuntimeException(e);
        }
        Frame<BasicValue>[] frames = analyzer.getFrames();
        AbstractInsnNode[] insns = mn.instructions.toArray();

        Value returnedCompositeValue = getReturnedCompositeValue(frames, insns);

        return new Result(context.getUsedFields(), returnedCompositeValue);
    }

    private static Value getReturnedCompositeValue(Frame<BasicValue>[] frames, AbstractInsnNode[] insns) {
        Set<Value> resultValues = new HashSet<>();

        for (int i = 0; i < insns.length; i++) {
            AbstractInsnNode insn = insns[i];
            switch (insn.getOpcode()) {
                case IRETURN:
                case LRETURN:
                case FRETURN:
                case DRETURN:
                case ARETURN:
                    BasicValue value = frames[i].getStack(0);
                    if (Value.isComposite(value)) {
                        resultValues.add((Value) value);
                    }
                    break;
            }
        }

        if (resultValues.isEmpty())
            return null;

        return mergeValues(resultValues);
    }
}
