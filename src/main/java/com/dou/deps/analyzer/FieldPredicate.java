package com.dou.deps.analyzer;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

import static java.util.Arrays.asList;
import static java.util.Arrays.stream;

public interface FieldPredicate {

    boolean test(Field field);

    @SafeVarargs
    static FieldPredicate withAnnotations(Class<? extends Annotation>... supportedAnnotations) {
        Set<Class<? extends Annotation>> supported = new HashSet<>(asList(supportedAnnotations));
        return field -> stream(field.getAnnotations()).map(Annotation::annotationType).anyMatch(supported::contains);
    }

    static FieldPredicate all() {
        return field -> true;
    }
}
