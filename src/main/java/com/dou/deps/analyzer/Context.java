package com.dou.deps.analyzer;

import com.dou.deps.value.Value;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

public class Context {

    private final Method currentMethod;
    private final int classAccess;
    private final Set<Value> usedFields;

    public Context(Method currentMethod, int classAccess) {
        this.currentMethod = currentMethod;
        this.classAccess = classAccess;
        this.usedFields = new HashSet<>();
    }

    public Class<?> getCurrentClass() {
        return currentMethod.getDeclaringClass();
    }

    public int getClassAccess() {
        return classAccess;
    }

    public Set<Value> getUsedFields() {
        return usedFields;
    }

    public void addUsedField(Value usedField) {
        usedFields.add(usedField);
    }

    @Override
    public String toString() {
        return currentMethod.toString();
    }
}
