package com.dou.deps.analyzer;

import com.dou.deps.value.Value;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.*;
import org.objectweb.asm.tree.analysis.AnalyzerException;
import org.objectweb.asm.tree.analysis.BasicInterpreter;
import org.objectweb.asm.tree.analysis.BasicValue;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.dou.deps.analyzer.MethodAnalyzer.analyzeFields;
import static com.dou.deps.utils.AsmUtils.reflectClass;
import static com.dou.deps.utils.LookupUtil.*;
import static com.dou.deps.utils.ResolutionUtil.*;
import static com.dou.deps.value.Ref.thisRef;
import static com.dou.deps.value.Value.*;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.objectweb.asm.Type.*;
import static org.objectweb.asm.tree.AbstractInsnNode.VAR_INSN;
import static org.objectweb.asm.tree.analysis.BasicValue.UNINITIALIZED_VALUE;

public class FieldsInterpreter extends BasicInterpreter {

    private final Context context;
    private final Configuration configuration;

    private boolean wasUpdated = false;

    public FieldsInterpreter(Context context, Configuration configuration) {
        super(ASM6);
        this.context = context;
        this.configuration = configuration;
    }

    @Override
    public BasicValue newValue(Type type) {
        if (type != null && type.getSort() == OBJECT)
            return new BasicValue(type);
        return super.newValue(type);
    }

    @Override
    public BasicValue copyOperation(AbstractInsnNode insn, BasicValue value) throws AnalyzerException {
        if (wasUpdated || insn.getType() != VAR_INSN || ((VarInsnNode) insn).var != 0) {
            return super.copyOperation(insn, value);
        }

        switch (insn.getOpcode()) {
            case ALOAD:
                return typedValue(value.getType(), thisRef());
            case ISTORE:
            case LSTORE:
            case FSTORE:
            case DSTORE:
            case ASTORE:
                wasUpdated = true;
        }

        return super.copyOperation(insn, value);
    }

    @Override
    public BasicValue unaryOperation(AbstractInsnNode insn, BasicValue value) throws AnalyzerException {
        if (Value.isComposite(value)) {
            switch (insn.getOpcode()) {
                case PUTSTATIC: {
                    throw new IllegalStateException("could not trace " + value + " over putstatic");
                }
                case GETFIELD: {
                    Optional<Value> optionalFieldValue = childValue((Value) value, (FieldInsnNode) insn, configuration);
                    if (!optionalFieldValue.isPresent())
                        break;

                    Value fieldValue = optionalFieldValue.get();

                    if (configuration.isInterestingField(resolveField((FieldInsnNode) insn))) {
                        context.addUsedField(fieldValue);
                    }

                    if (Value.isComposite(fieldValue)) {
                        return fieldValue;
                    }
                    break;
                }
                case CHECKCAST: {
                    Class<?> original = reflectClass(value.getType());
                    Type targetType = getObjectType(((TypeInsnNode) insn).desc);
                    Class<?> afterCast = reflectClass(targetType);
                    if (afterCast.isAssignableFrom(original)) {
                        return value;
                    } else {
                        throw new IllegalStateException("type specification not supported");
                    }
                }
            }
        }

        return super.unaryOperation(insn, value);
    }

    @Override
    public BasicValue binaryOperation(AbstractInsnNode insn, BasicValue value1, BasicValue value2) throws AnalyzerException {
        if (insn.getOpcode() == PUTFIELD && Value.isComposite(value2)) {
            throw new IllegalStateException("could not trace " + value2 + " over putfield");
        }
        return super.binaryOperation(insn, value1, value2);
    }

    @Override
    public BasicValue ternaryOperation(AbstractInsnNode insn, BasicValue value1, BasicValue value2, BasicValue value3) throws AnalyzerException {
        if (insn.getOpcode() == AASTORE && Value.isComposite(value3)) {
            throw new IllegalStateException("could not trace " + value3 + " over aastore");
        }
        return super.ternaryOperation(insn, value1, value2, value3);
    }

    @Override
    public BasicValue naryOperation(AbstractInsnNode insn, List<? extends BasicValue> values) throws AnalyzerException {
        Method method = null;
        Value methodThis = null;

        switch (insn.getOpcode()) {
            case INVOKESPECIAL: {
                List<?> badValues = values.stream().skip(1).filter(Value::isComposite).collect(toList());
                if (!badValues.isEmpty())
                    throw new IllegalStateException("could not pass " + badValues + " as parameter");

                if (Value.isComposite(values.get(0))) {
                    MethodInsnNode methodNode = (MethodInsnNode) insn;

                    Class<?> clazz = reflectClass(methodNode.owner);

                    Method resolvedMethod;
                    if (clazz.isInterface())
                        resolvedMethod = resolveInterfaceMethod(clazz, methodNode.name, getMethodType(methodNode.desc));
                    else
                        resolvedMethod = resolveMethod(clazz, methodNode.name, getMethodType(methodNode.desc));

                    boolean isInstanceInitializationMethod = !resolvedMethod.getDeclaringClass().isInterface()
                            && resolvedMethod.getName().equals("<init>")
                            && resolvedMethod.getReturnType().equals(void.class);

                    Class<?> targetClass;

                    //See JVMS invokespecial
                    if (!isInstanceInitializationMethod
                            && isSubclass(context.getCurrentClass(), clazz)
                            && (context.getClassAccess() & ACC_SUPER) != 0) {
                        targetClass = context.getCurrentClass().getSuperclass();
                    } else {
                        targetClass = clazz;
                    }

                    method = lookupSpecialMethod(targetClass, resolvedMethod);
                    methodThis = (Value) values.get(0);
                }

                break;
            }
            case INVOKEVIRTUAL: {
                if (Value.isComposite(values.get(0))) {
                    MethodInsnNode methodNode = (MethodInsnNode) insn;

                    Class<?> objectClass = reflectClass(values.get(0).getType());

                    Method virtualMethod = resolveMethod(reflectClass(methodNode.owner), methodNode.name, getMethodType(methodNode.desc));

                    method = lookupVirtualMethod(objectClass, virtualMethod);
                    methodThis = (Value) values.get(0);
                }

                List<?> badValues = values.stream().skip(1).filter(Value::isComposite).collect(toList());
                if (!badValues.isEmpty())
                    throw new IllegalStateException("could not pass " + badValues + " as parameter");
                break;
            }
            case INVOKEINTERFACE: {
                if (Value.isComposite(values.get(0))) {
                    MethodInsnNode methodNode = (MethodInsnNode) insn;

                    Class<?> objectClass = reflectClass(values.get(0).getType());

                    Method interfaceMethod = resolveInterfaceMethod(reflectClass(methodNode.owner), methodNode.name, getMethodType(methodNode.desc));

                    method = lookupInterfaceMethod(objectClass, interfaceMethod);
                    methodThis = (Value) values.get(0);
                }

                List<?> badValues = values.stream().skip(1).filter(Value::isComposite).collect(toList());
                if (!badValues.isEmpty())
                    throw new IllegalStateException("could not pass " + badValues + " as parameter");
                break;
            }
            case INVOKESTATIC:
            case INVOKEDYNAMIC: {
                List<?> badValues = values.stream().filter(Value::isComposite).collect(toList());
                if (!badValues.isEmpty())
                    throw new IllegalStateException("could not pass " + badValues + " as parameter");
                break;
            }
        }

        if (method != null) {
            MethodAnalyzer.Result methodResult = analyzeFields(method, configuration);

            for (Value usedField : methodResult.getUsedFields()) {
                childValue(methodThis, usedField).ifPresent(context::addUsedField);
            }

            if (methodResult.getReturnedCompositeValue() != null) {
                Optional<Value> returnedValue = childValue(methodThis, methodResult.getReturnedCompositeValue());
                if (returnedValue.isPresent()) {
                    return returnedValue.get();
                }
            }
        }

        return super.naryOperation(insn, values);
    }

    @Override
    public BasicValue merge(BasicValue v, BasicValue w) {
        if (v.equals(w))
            return v;
        if (v instanceof Value || w instanceof Value) {
            if (!Objects.equals(v.getType(), w.getType())) {
                if (v == UNINITIALIZED_VALUE || w == UNINITIALIZED_VALUE)
                    return UNINITIALIZED_VALUE;
                throw new IllegalStateException("could not merge " + v + " and " + w);
            }

            if (v instanceof Value != w instanceof Value) {
                if (v instanceof Value)
                    return v;
                else
                    return w;
            }

            return mergeValues(asList((Value) v, (Value) w));
        }
        return super.merge(v, w);
    }

    @Override
    public String toString() {
        return context.toString();
    }
}
