package com.dou.deps.utils;

import org.objectweb.asm.Type;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

import static java.util.Arrays.stream;
import static org.objectweb.asm.Type.*;

public class AsmUtils {

    public static Field reflectFieldOrNull(Class<?> clazz, String name, Type descriptor) {
        Class<?> fieldType = reflectClass(descriptor);

        for (Field field : clazz.getDeclaredFields()) {
            if (!field.getName().equals(name))
                continue;
            if (!field.getType().equals(fieldType))
                continue;
            return field;
        }

        return null;
    }

    public static Method reflectMethodOrNull(Class<?> clazz, String name, Type descriptor) {
        Class<?>[] argumentTypes = stream(descriptor.getArgumentTypes()).map(AsmUtils::reflectClass).toArray(Class[]::new);
        Class<?> returnType = reflectClass(descriptor.getReturnType());

        for (Method method : clazz.getDeclaredMethods()) {
            if (!method.getName().equals(name))
                continue;
            if (!Arrays.equals(method.getParameterTypes(), argumentTypes))
                continue;
            if (!method.getReturnType().equals(returnType))
                continue;
            return method;
        }

        return null;
    }

    public static Class<?> reflectClass(String internalName) {
        try {
            return Class.forName(internalName.replace('/', '.'));
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static Class<?> reflectClass(Type type) {
        switch (type.getSort()) {
            case VOID:
                return void.class;
            case BOOLEAN:
                return boolean.class;
            case CHAR:
                return char.class;
            case BYTE:
                return byte.class;
            case SHORT:
                return short.class;
            case INT:
                return int.class;
            case FLOAT:
                return float.class;
            case LONG:
                return long.class;
            case DOUBLE:
                return double.class;
            case ARRAY:
                Class<?> componentType = reflectClass(type.getElementType());
                int[] dimensions = (int[]) Array.newInstance(int.class, type.getDimensions());
                return Array.newInstance(componentType, dimensions).getClass();
            case OBJECT:
                return reflectClass(type.getInternalName());
            default:
                throw new AssertionError(type);
        }
    }
}
