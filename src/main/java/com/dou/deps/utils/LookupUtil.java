package com.dou.deps.utils;

import org.objectweb.asm.Type;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;

import static com.dou.deps.utils.AsmUtils.reflectMethodOrNull;
import static com.dou.deps.utils.ResolutionUtil.*;
import static java.lang.reflect.Modifier.*;
import static java.util.stream.Collectors.toList;
import static org.objectweb.asm.Type.getType;

public class LookupUtil {

    /**
     * See JVMS invokespecial
     */
    public static Method lookupSpecialMethod(Class<?> targetClass, Method specialMethod) {
        Class<?> clazz = targetClass;
        while (clazz != null) {
            Method method = findByNameAndDescriptionOrNull(clazz, specialMethod);
            if (method != null && !isStatic(method.getModifiers()))
                return method;
            clazz = clazz.getSuperclass();
        }

        String name = specialMethod.getName();
        Type descriptor = getType(specialMethod);

        if (targetClass.isInterface()) {
            Method method = reflectMethodOrNull(Object.class, name, descriptor);
            if (method != null && isPublic(method.getModifiers()) && isStatic(method.getModifiers()))
                return method;
        }

        Set<Method> interfaceMethods = getMaximallySpecificSuperinterfaceMethods(targetClass, name, descriptor);

        List<Method> methods = interfaceMethods.stream().filter(m -> !isAbstract(m.getModifiers())).collect(toList());
        if (methods.size() == 1)
            return methods.get(0);

        throw new IllegalStateException("special method lookup failed: " + targetClass + "." + name + descriptor);
    }

    /**
     * See JVMS invokevirtual
     */
    public static Method lookupVirtualMethod(Class<?> objectClass, Method virtualMethod) {
        Class<?> clazz = objectClass;
        while (clazz != null) {
            Method method = findByNameAndDescriptionOrNull(clazz, virtualMethod);
            if (method != null && !isStatic(method.getModifiers()) && isOverrides(method, virtualMethod))
                return method;
            clazz = clazz.getSuperclass();
        }

        String name = virtualMethod.getName();
        Type descriptor = getType(virtualMethod);

        Set<Method> interfaceMethods = getMaximallySpecificSuperinterfaceMethods(objectClass, name, descriptor);

        List<Method> methods = interfaceMethods.stream().filter(m -> !isAbstract(m.getModifiers())).collect(toList());
        if (methods.size() == 1)
            return methods.get(0);

        throw new IllegalStateException("virtual method lookup failed: " + objectClass + "." + name + descriptor);
    }

    /**
     * See JVMS invokeinterface
     */
    public static Method lookupInterfaceMethod(Class<?> objectClass, Method interfaceMethod) {
        Class<?> clazz = objectClass;
        while (clazz != null) {
            Method method = findByNameAndDescriptionOrNull(clazz, interfaceMethod);
            if (method != null && !isStatic(method.getModifiers()))
                return method;
            clazz = clazz.getSuperclass();
        }

        String name = interfaceMethod.getName();
        Type descriptor = getType(interfaceMethod);

        Set<Method> interfaceMethods = getMaximallySpecificSuperinterfaceMethods(objectClass, name, descriptor);

        List<Method> methods = interfaceMethods.stream().filter(m -> !isAbstract(m.getModifiers())).collect(toList());
        if (methods.size() == 1)
            return methods.get(0);

        throw new IllegalStateException("interface method lookup failed: " + objectClass + "." + name + descriptor);
    }
}
