package com.dou.deps.utils;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.tree.MethodNode;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;

import static org.objectweb.asm.ClassReader.SKIP_DEBUG;
import static org.objectweb.asm.ClassReader.SKIP_FRAMES;
import static org.objectweb.asm.Opcodes.ASM6;
import static org.objectweb.asm.Type.getInternalName;
import static org.objectweb.asm.Type.getMethodDescriptor;

public class MethodReader {

    public static class MethodInfo {
        private final String internalDeclaringClassName;
        private final int classAccess;
        private final MethodNode methodNode;

        public MethodInfo(String internalDeclaringClassName, int classAccess, MethodNode methodNode) {
            this.internalDeclaringClassName = internalDeclaringClassName;
            this.classAccess = classAccess;
            this.methodNode = methodNode;
        }

        public String getInternalDeclaringClassName() {
            return internalDeclaringClassName;
        }

        public int getClassAccess() {
            return classAccess;
        }

        public MethodNode getMethodNode() {
            return methodNode;
        }
    }

    public static MethodInfo readMethod(Method method) {
        Class<?> clazz = method.getDeclaringClass();
        String internalClassName = getInternalName(clazz);

        try (InputStream is = getClassFile(clazz)) {
            ClassReader cr = new ClassReader(is);
            AnalyzerClassVisitor cv = new AnalyzerClassVisitor(internalClassName, method);
            cr.accept(cv, SKIP_DEBUG | SKIP_FRAMES);
            return new MethodInfo(internalClassName, cv.getAccess(), cv.getMethodNode());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static InputStream getClassFile(Class<?> clazz) {
        String file = clazz.getName().replace('.', '/') + ".class";

        ClassLoader cl = clazz.getClassLoader();
        if (cl == null)
            return ClassLoader.getSystemResourceAsStream(file);
        else
            return cl.getResourceAsStream(file);
    }

    private static class AnalyzerClassVisitor extends ClassVisitor {

        private final String className;
        private final String getterName;
        private final String getterDesc;

        private MethodNode methodNode;
        private int access;

        public AnalyzerClassVisitor(String internalClassName, Method getter) {
            super(ASM6);
            this.className = internalClassName;
            this.getterName = getter.getName();
            this.getterDesc = getMethodDescriptor(getter);
        }

        public MethodNode getMethodNode() {
            if (methodNode == null)
                throw new IllegalStateException();
            return methodNode;
        }

        public int getAccess() {
            return access;
        }

        @Override
        public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
            if (!name.equals(className))
                throw new IllegalStateException();

            this.access = access;
        }

        @Override
        public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
            if (!name.equals(getterName) || !desc.equals(getterDesc))
                return null;
            return new AnalyzerMethodVisitor(access, name, desc, signature, exceptions);
        }

        private class AnalyzerMethodVisitor extends MethodVisitor {

            public AnalyzerMethodVisitor(int access, String name, String desc, String signature, String[] exceptions) {
                super(ASM6, new MethodNode(ASM6, access, name, desc, signature, exceptions));
            }

            @Override
            public void visitEnd() {
                if (methodNode != null)
                    throw new IllegalStateException();
                methodNode = (MethodNode) mv;
            }
        }
    }
}
