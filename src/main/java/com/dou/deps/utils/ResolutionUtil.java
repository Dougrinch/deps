package com.dou.deps.utils;

import org.objectweb.asm.Type;
import org.objectweb.asm.tree.FieldInsnNode;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.dou.deps.utils.AsmUtils.*;
import static java.lang.reflect.Modifier.*;
import static java.util.stream.Collectors.toList;

public class ResolutionUtil {

    public static Field resolveField(FieldInsnNode node) {
        Class<?> clazz = reflectClass(node.owner);
        Type descriptor = Type.getType(node.desc);
        return resolveField(clazz, node.name, descriptor);
    }

    /**
     * See JVMS 5.4.3.2. Field Resolution
     */
    public static Field resolveField(Class<?> clazz, String name, Type descriptor) {
        Field field = reflectFieldOrNull(clazz, name, descriptor);
        if (field != null)
            return field;

        for (Class<?> iface : clazz.getInterfaces()) {
            Field interfaceField = resolveField(iface, name, descriptor);
            if (interfaceField != null)
                return interfaceField;
        }

        if (clazz.getSuperclass() != null) {
            Field superField = resolveField(clazz.getSuperclass(), name, descriptor);
            if (superField != null)
                return superField;
        }

        throw new IllegalStateException("field resolve failed: " + clazz + "." + name + descriptor);
    }

    /**
     * See JVMS 5.4.3.3. Method Resolution
     */
    public static Method resolveMethod(Class<?> clazz, String name, Type descriptor) {
        if (clazz.isInterface())
            throw new IllegalStateException("method resolve failed: " + clazz + "." + name + descriptor);

        Class<?> cl = clazz;
        while (cl != null) {
            Method method = reflectMethodOrNull(cl, name, descriptor);
            if (method != null)
                return method;
            cl = cl.getSuperclass();
        }

        Set<Method> interfaceMethods = getMaximallySpecificSuperinterfaceMethods(clazz, name, descriptor);

        List<Method> methods = interfaceMethods.stream().filter(m -> !isAbstract(m.getModifiers())).collect(toList());
        if (methods.size() == 1)
            return methods.get(0);

        if (!interfaceMethods.isEmpty())
            return interfaceMethods.iterator().next();

        throw new IllegalStateException("method resolve failed: " + clazz + "." + name + descriptor);
    }

    /**
     * See JVMS 5.4.3.4. Interface Method Resolution
     */
    public static Method resolveInterfaceMethod(Class<?> clazz, String name, Type descriptor) {
        if (!clazz.isInterface())
            throw new IllegalStateException("method resolve failed: " + clazz + "." + name + descriptor);

        Method method = reflectMethodOrNull(clazz, name, descriptor);
        if (method != null)
            return method;

        Method objectMethod = reflectMethodOrNull(Object.class, name, descriptor);
        if (objectMethod != null && isPublic(objectMethod.getModifiers()) && !isStatic(objectMethod.getModifiers()))
            return objectMethod;

        Set<Method> interfaceMethods = getMaximallySpecificSuperinterfaceMethods(clazz, name, descriptor);

        List<Method> methods = interfaceMethods.stream().filter(m -> !isAbstract(m.getModifiers())).collect(toList());
        if (methods.size() == 1)
            return methods.get(0);

        if (!interfaceMethods.isEmpty())
            return interfaceMethods.iterator().next();

        throw new IllegalStateException("method resolve failed: " + clazz + "." + name + descriptor);
    }

    /**
     * See JVMS 5.4.3.3. Method Resolution
     */
    public static Set<Method> getMaximallySpecificSuperinterfaceMethods(Class<?> clazz, String name, Type descriptor) {
        Set<Method> result = new HashSet<>();
        collectAllSuperinterfaceMethods(clazz, name, descriptor, result);
        result.removeIf(m -> result.stream().anyMatch(sm -> sm != m && m.getDeclaringClass().isAssignableFrom(sm.getDeclaringClass())));
        return result;
    }

    private static void collectAllSuperinterfaceMethods(Class<?> clazz, String name, Type descriptor, Set<Method> result) {
        for (Class<?> iface : clazz.getInterfaces()) {
            Method method = reflectMethodOrNull(iface, name, descriptor);
            if (method != null && !isPrivate(method.getModifiers()) && !isStatic(method.getModifiers())) {
                result.add(method);
            }
            collectAllSuperinterfaceMethods(iface, name, descriptor, result);
        }
    }

    /**
     * See JVMS 5.4.5. Overriding
     */
    public static boolean isOverrides(Method child, Method parent) {
        if (child.equals(parent))
            return true;
        if (!isSubclass(child.getDeclaringClass(), parent.getDeclaringClass()))
            return false;
        if (!isSameNameAndDescription(child, parent))
            return false;
        if (isPrivate(child.getModifiers()))
            return false;

        if (isPublic(parent.getModifiers()))
            return true;
        if (isProtected(parent.getModifiers()))
            return true;
        if (!isPublic(parent.getModifiers()) && !isProtected(parent.getModifiers()) && !isPrivate(parent.getModifiers())) {
            if (child.getDeclaringClass().getPackage().equals(parent.getDeclaringClass().getPackage()))
                return true;
        }

        Class<?> clazz = child.getDeclaringClass().getSuperclass();
        while (clazz != null && isSubclass(clazz, parent.getDeclaringClass())) {
            Method method = findByNameAndDescriptionOrNull(clazz, parent);
            if (method != null && !method.equals(child) && !method.equals(parent)) {
                if (isOverrides(child, method) && isOverrides(method, parent)) {
                    return true;
                }
            }
            clazz = clazz.getSuperclass();
        }

        return false;
    }

    public static Method findByNameAndDescriptionOrNull(Class<?> clazz, Method method) {
        for (Method result : clazz.getDeclaredMethods()) {
            if (isSameNameAndDescription(result, method))
                return result;
        }
        return null;
    }

    private static boolean isSameNameAndDescription(Method m1, Method m2) {
        if (!m1.getName().equals(m2.getName()))
            return false;
        if (!Arrays.equals(m1.getParameterTypes(), m2.getParameterTypes()))
            return false;
        if (!m1.getReturnType().equals(m2.getReturnType()))
            return false;
        return true;
    }

    public static boolean isSubclass(Class<?> child, Class<?> parent) {
        return !child.equals(parent) && !parent.isInterface() && parent.isAssignableFrom(child);
    }
}
