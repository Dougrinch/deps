package com.dou.deps.value;

import com.dou.deps.analyzer.Configuration;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static java.util.Collections.emptyList;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.stream.Collectors.joining;

public class Ref {

    private final List<Field> path;
    private final boolean composite;

    public Ref(List<Field> path, boolean composite) {
        this.path = path;
        this.composite = composite;
    }

    public List<Field> getPath() {
        return path;
    }

    public boolean isComposite() {
        return composite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ref ref = (Ref) o;
        return Objects.equals(path, ref.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path);
    }

    @Override
    public String toString() {
        if (path.isEmpty())
            return "<[this]>";
        else
            return "<" + path.stream().map(Field::getName).collect(joining(".")) + ">";
    }

    public static Ref thisRef() {
        return new Ref(emptyList(), true);
    }

    public static Optional<Ref> childRef(Ref parent, Field field, Configuration configuration) {
        if (!parent.isComposite())
            return empty();
        if (parent.path.contains(field))
            return empty();

        List<Field> path = new ArrayList<>(parent.path);
        path.add(field);
        return of(new Ref(path, configuration.isCompositeField(field)));
    }

    public static Optional<Ref> childRef(Ref parent, Ref child) {
        if (!parent.isComposite())
            return empty();
        if (child.path.stream().anyMatch(parent.path::contains))
            return empty();

        List<Field> path = new ArrayList<>(parent.path);
        path.addAll(child.path);
        return of(new Ref(path, child.composite));
    }
}
