package com.dou.deps.value;

import com.dou.deps.analyzer.Configuration;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.analysis.BasicValue;

import java.lang.reflect.Field;
import java.util.*;

import static com.dou.deps.utils.ResolutionUtil.resolveField;
import static com.dou.deps.value.Ref.childRef;
import static java.util.Collections.singleton;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.stream.Collectors.*;

public class Value extends BasicValue {

    private final Set<Ref> refs;

    private Value(Type type, Set<Ref> refs) {
        super(type);
        this.refs = refs;
    }

    public Set<Ref> getRefs() {
        return refs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Value value = (Value) o;
        return Objects.equals(refs, value.refs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), refs);
    }

    @Override
    public String toString() {
        return "(" + refs.stream().map(Object::toString).collect(joining(",")) + ")";
    }

    public static Value typedValue(Type type, Ref ref) {
        return new Value(type, singleton(ref));
    }

    public static Optional<Value> childValue(Value parent, Value child) {
        Type type = child.getType();
        Set<Ref> fields = parent.refs.stream()
                .flatMap(p -> child.refs.stream().map(c -> childRef(p, c)))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(toSet());
        if (fields.isEmpty())
            return empty();
        return of(new Value(type, fields));
    }

    public static Optional<Value> childValue(Value parent, FieldInsnNode childInsn, Configuration configuration) {
        Type type = Type.getType(childInsn.desc);
        Field child = resolveField(childInsn);
        Set<Ref> fields = parent.refs.stream()
                .map(p -> childRef(p, child, configuration))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(toSet());
        if (fields.isEmpty())
            return empty();
        return of(new Value(type, fields));
    }

    public static Value mergeValues(Collection<Value> values) {
        List<Type> types = values.stream().map(BasicValue::getType).distinct().collect(toList());
        if (types.size() != 1) {
            String typesAsString = types.stream().map(Type::toString).collect(joining(", ", "(", ")"));
            throw new IllegalStateException("could not merge " + typesAsString);
        }

        Set<Ref> fields = values.stream().flatMap(v -> v.refs.stream()).distinct().collect(toSet());
        return new Value(types.get(0), fields);
    }

    public static boolean isComposite(BasicValue value) {
        return value instanceof Value && value.getType().getSort() == Type.OBJECT && ((Value) value).refs.stream().anyMatch(Ref::isComposite);
    }
}
