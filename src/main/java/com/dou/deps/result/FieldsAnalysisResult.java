package com.dou.deps.result;

import com.dou.deps.analyzer.MethodAnalyzer;
import com.dou.deps.value.Ref;
import com.dou.deps.value.Value;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toList;

public class FieldsAnalysisResult {

    private final Set<Ref> returnedCompositeRefs;
    private final FieldTree usedFields;

    public FieldsAnalysisResult(MethodAnalyzer.Result result) {
        usedFields = new FieldTree(result.getUsedFields());
        returnedCompositeRefs = new HashSet<>();
        addReturnedValue(result.getReturnedCompositeValue());
    }

    private void addReturnedValue(Value value) {
        if (value == null)
            return;

        returnedCompositeRefs.addAll(value.getRefs());
    }

    public Set<Ref> getReturnedCompositeRefs() {
        return returnedCompositeRefs;
    }

    public FieldTree getUsedFields() {
        return usedFields;
    }

    @Override
    public String toString() {
        List<String> returned = returnedCompositeRefs.stream().map(Ref::toString).collect(toList());
        return "FieldsAnalysisResult{\n" +
                "   returnedCompositeRefs=" + returned + "\n" +
                "   usedFields=" + usedFields.toString("              ") + "\n" +
                '}';
    }
}
