package com.dou.deps.result;

import com.dou.deps.value.Ref;
import com.dou.deps.value.Value;

import java.lang.reflect.Field;
import java.util.*;
import java.util.Map.Entry;

public class FieldTree {

    private final Map<Field, FieldTree> children;

    public FieldTree() {
        this.children = new HashMap<>();
    }

    public FieldTree(Set<Value> values) {
        children = new HashMap<>();

        for (Value value : values) {
            add(value);
        }
    }

    private void add(Value value) {
        for (Ref ref : value.getRefs()) {
            FieldTree tree = this;
            for (Field field : ref.getPath()) {
                tree = tree.getOrAdd(field);
            }
        }
    }

    public Map<Field, FieldTree> getChildren() {
        return children;
    }

    public FieldTree getOrAdd(Field child) {
        return children.computeIfAbsent(child, f -> new FieldTree());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FieldTree fieldTree = (FieldTree) o;
        return Objects.equals(children, fieldTree.children);
    }

    @Override
    public int hashCode() {
        return Objects.hash(children);
    }

    @Override
    public String toString() {
        return toString("");
    }

    public String toString(String offset) {
        StringBuilder buf = new StringBuilder();
        toString(offset, buf);
        return buf.toString();
    }

    public void toString(String offset, StringBuilder buf) {
        Iterator<Entry<Field, FieldTree>> it = children.entrySet().iterator();

        if (it.hasNext()) {
            Entry<Field, FieldTree> entry = it.next();
            if (it.hasNext()) {
                buf.append(" ─┬─ ");
            } else {
                buf.append(" ─── ");
            }
            buf.append(entry.getKey().getName());

            if (!entry.getValue().getChildren().isEmpty()) {
                @SuppressWarnings("ReplaceAllDot")
                String childOffset = offset + (it.hasNext() ? "  │  " : "     ") + entry.getKey().getName().replaceAll(".", " ");
                entry.getValue().toString(childOffset, buf);
            }
        }

        while (it.hasNext()) {
            buf.append('\n');

            Entry<Field, FieldTree> entry = it.next();
            buf.append(offset);

            if (it.hasNext()) {
                buf.append("  ├─ ");
            } else {
                buf.append("  └─ ");
            }

            buf.append(entry.getKey().getName());

            if (!entry.getValue().getChildren().isEmpty()) {
                @SuppressWarnings("ReplaceAllDot")
                String childOffset = offset + (it.hasNext() ? "  │  " : "     ") + entry.getKey().getName().replaceAll(".", " ");
                entry.getValue().toString(childOffset, buf);
            }
        }
    }
}
